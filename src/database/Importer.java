package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.postgresql.util.PSQLException;

import helper.CSVLoader;
import helper.ColumnType;
import bean.StopPoint;

public class Importer {
	
	//Import all files
	public boolean importAgency(String filename) {
		
		Connection connection = null;
		try {
			connection = DBConnection.openNewConnection();
			CSVLoader loader = new CSVLoader(connection);
			loader.loadCSV(filename, Schema.AGENCY, false, ColumnType.String, ColumnType.String,
							ColumnType.String, ColumnType.String, ColumnType.String, ColumnType.Integer);
			
		} catch (SQLException e1) {
			e1.printStackTrace();
			e1.getNextException().printStackTrace();;
			System.out.println("Failed to open connection to database to import " + filename);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Failed to import " + filename + " to database " + Configurations.DATABASE_PATH);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public boolean importRoutes(String filename) {
		Connection connection = null;
		try {
			connection = DBConnection.openNewConnection();
			CSVLoader loader = new CSVLoader(connection);
			loader.loadCSV(filename, Schema.ROUTES, false, ColumnType.String, ColumnType.String, 
					ColumnType.String, ColumnType.String, ColumnType.String, ColumnType.Integer, 
					ColumnType.String, ColumnType.String);
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println("Failed to open connection to database to import " + filename);
		} catch (Exception e) {
			System.out.println("Failed to import " + filename + " to database " + Configurations.DATABASE_PATH);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public boolean importTrips(String filename) {
		Connection connection = null;
		try {
			connection = DBConnection.openNewConnection();
			CSVLoader loader = new CSVLoader(connection);
			loader.loadCSV(filename, Schema.TRIPS, false, ColumnType.String, ColumnType.String, 
					ColumnType.String, ColumnType.String, ColumnType.String, ColumnType.Integer, 
					ColumnType.String, ColumnType.String);
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println("Failed to open connection to database to import " + filename);
		} catch (Exception e) {
			System.out.println("Failed to import " + filename + " to database " + Configurations.DATABASE_PATH);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public boolean importStops(String filename) {
		Connection connection = null;
		try {
			connection = DBConnection.openNewConnection();
			CSVLoader loader = new CSVLoader(connection);
			loader.loadCSV(filename, Schema.STOPS, false, ColumnType.String, ColumnType.String, 
					ColumnType.String, ColumnType.Float, ColumnType.Float, ColumnType.Integer, 
					ColumnType.String, ColumnType.Integer, ColumnType.String);

			connection = DBConnection.openNewConnection();
			PreparedStatement stmt = connection.prepareStatement("ALTER TABLE STOPS ADD FOREIGN KEY (parent_station) REFERENCES STOPS(stop_id)");
			stmt.executeUpdate();
			stmt.close();
			connection.close();

		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println("Failed to open connection to database to import " + filename);
		} catch (Exception e) {
			System.out.println("Failed to import " + filename + " to database " + Configurations.DATABASE_PATH);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public boolean importStopTimes(String filename) {
		Connection connection = null;
		try {
			connection = DBConnection.openNewConnection();
			CSVLoader loader = new CSVLoader(connection);
			loader.loadCSV(filename, Schema.STOP_TIMES, false, ColumnType.String, ColumnType.Time, 
					ColumnType.Time, ColumnType.String, ColumnType.Integer, ColumnType.String,
					ColumnType.Integer, ColumnType.Integer, ColumnType.Float);
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println("Failed to open connection to database to import " + filename);
		} catch (Exception e) {
			System.out.println("Failed to import " + filename + " to database " + Configurations.DATABASE_PATH);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public boolean importCalendar(String filename) {
		Connection connection = null;
		try {
			connection = DBConnection.openNewConnection();
			CSVLoader loader = new CSVLoader(connection);
			loader.loadCSV(filename, Schema.CALENDAR, false, ColumnType.String, ColumnType.Integer,
					ColumnType.Integer, ColumnType.Integer, ColumnType.Integer, ColumnType.Integer,
					ColumnType.Integer, ColumnType.Integer, ColumnType.Date, ColumnType.Date);
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println("Failed to open connection to database to import " + filename);
		} catch (Exception e) {
			System.out.println("Failed to import " + filename + " to database " + Configurations.DATABASE_PATH);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public boolean importCalendarDates(String filename) {
		Connection connection = null;
		try {
			connection = DBConnection.openNewConnection();
			CSVLoader loader = new CSVLoader(connection);
			loader.loadCSV(filename, Schema.CALENDAR_DATES, false, ColumnType.String, ColumnType.Date,
					ColumnType.Integer);
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println("Failed to open connection to database to import " + filename);
		} catch (Exception e) {
			System.out.println("Failed to import " + filename + " to database " + Configurations.DATABASE_PATH);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	public boolean importShapes(String filename) {
		Connection connection = null;
		try {
			connection = DBConnection.openNewConnection();
			CSVLoader loader = new CSVLoader(connection);
			loader.loadCSV(filename, Schema.SHAPES, false, ColumnType.String, 
					ColumnType.Float, ColumnType.Float,
					ColumnType.Integer, ColumnType.Float);
		} catch (SQLException e1) {
			e1.printStackTrace();
			System.out.println("Failed to open connection to database to import " + filename);
		} catch (Exception e) {
			System.out.println("Failed to import " + filename + " to database " + Configurations.DATABASE_PATH);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public void importSuburbs(HashSet<StopPoint> stops) {
		Connection connection = null;
		PreparedStatement stmt = null;
		try {
			connection = DBConnection.openNewConnection();
			
			// create temporary tables so that we can have suburbs-stop_id relation without foreign
			// key exception due to the suburbs table has not been filled with newest data
			// and/or the stops table is still empty

			System.out.println("Creating table " + Schema.SUBURBS_STOPS);
//			connection.setAutoCommit(false);
			stmt = connection.prepareStatement("CREATE TABLE IF NOT EXISTS " + Schema.SUBURBS_STOPS + "(suburb TEXT not null, stop_id TEXT primary key)");
			stmt.execute();
			stmt.close();
			System.out.println("Created table " + Schema.SUBURBS_STOPS);
			
			System.out.println("Importing suburbs. nSuburbs= " + stops.size());
			// Insert suburb, stop_id to the temporary table
			stmt = connection.prepareStatement("insert into " + Schema.SUBURBS_STOPS + " (suburb, stop_id) values(?,?)");
			Iterator<StopPoint> iterator = stops.iterator();
			StopPoint curStop = null;
			while(iterator.hasNext()) {
				curStop = iterator.next();
				stmt.setString(1, curStop.getSuburb());
				stmt.setString(2, curStop.getStopID());
				try {
					stmt.execute();
				} catch (PSQLException e) {
					// it catches here if there is duplicates
//					System.out.println(e.getMessage());
				}
			}
			stmt.close();
			
			// This section will get all the distinct suburbs and place it on the suburbs table
			// Get the distinct suburb name from the temporary table
			stmt = connection.prepareStatement("select distinct suburb from " + Schema.SUBURBS_STOPS);
			ResultSet suburbs = stmt.executeQuery();
			stmt = connection.prepareStatement("insert into " + Schema.SUBURBS + " (suburb_name) values(?)");
			while(suburbs.next()) {
				stmt.setString(1, suburbs.getString(1));
				try {
					stmt.execute();
				} catch (PSQLException e) {
					System.out.println(e.getMessage());
				}
			}
			stmt.close();
			
			// Insert the suburb_id to each row in the stops table accordingly
			stmt = connection.prepareStatement("update " + Schema.STOPS + " set suburb_id="
					+ "(select suburb_id from " + Schema.SUBURBS + " where suburb_name=?) where stop_id=?");
			iterator = stops.iterator();
			while(iterator.hasNext()) {
				curStop = iterator.next();
				stmt.setString(1, curStop.getSuburb());
				stmt.setString(2, curStop.getStopID());
				try {
					stmt.execute();
				} catch (PSQLException e) {
					System.err.println(e.getMessage());
				}
			}
			stmt.close();
//			connection.commit();
		} catch(Exception e) {
			e.printStackTrace();
			try {
				connection.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void updateSimpleId() {
		Connection connection = DBConnection.openNewConnection();
		updateSimpleIdAgency(connection);
		updateSimpleIdStop(connection);
		updateSimpleIdRoutes(connection);
		updateSimpleIdTrips(connection);
		updateSimpleIdServices(connection);
		updateSimpleIdShapes(connection);
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void updateSimpleIdAgency(Connection connection) {
		PreparedStatement stmt = null;
		
		try {
			stmt = connection.prepareStatement("select distinct agency_id from " + Schema.AGENCY
												+ " where agency_id not in (select agency_id from "
												+ Schema.SIMPLEID_AGENCY + " )");
			ResultSet agencies = stmt.executeQuery();
			stmt = connection.prepareStatement("insert into " + Schema.SIMPLEID_AGENCY + " (agency_id) values(?)");
			while(agencies.next()) {
				stmt.setString(1, agencies.getString(1));
				try {
					stmt.execute();
				} catch (PSQLException e) {
					System.out.println(e.getMessage());
				}
			}
			agencies.close();
			stmt.close();
		} catch (SQLException e1) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_AGENCY + ".\n" + e1.getMessage());
		} catch (Exception e) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_AGENCY + ".\n" + e.getMessage());
		}
	}

	
	private void updateSimpleIdStop(Connection connection) {
		PreparedStatement stmt = null;
		
		try {
			stmt = connection.prepareStatement("select distinct stop_id from " + Schema.STOPS
												+ " where stop_id not in (select stop_id from "
												+ Schema.SIMPLEID_STOPS + " )");
			ResultSet stops = stmt.executeQuery();
			stmt = connection.prepareStatement("insert into " + Schema.SIMPLEID_STOPS + " (stop_id) values(?)");
			while(stops.next()) {
				stmt.setString(1, stops.getString(1));
				try {
					stmt.execute();
				} catch (PSQLException e) {
					System.out.println(e.getMessage());
				}
			}
			stops.close();
			stmt.close();
		} catch (SQLException e1) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_STOPS + ".\n" + e1.getMessage());
		} catch (Exception e) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_STOPS + ".\n" + e.getMessage());
		}
	}
	
	private void updateSimpleIdRoutes(Connection connection) {
		PreparedStatement stmt = null;
		
		try {
			stmt = connection.prepareStatement("select distinct route_id from " + Schema.ROUTES
												+ " where route_id not in (select route_id from "
												+ Schema.SIMPLEID_ROUTES + " )");
			ResultSet routes = stmt.executeQuery();
			stmt = connection.prepareStatement("insert into " + Schema.SIMPLEID_ROUTES + " (route_id) values(?)");
			while(routes.next()) {
				stmt.setString(1, routes.getString(1));
				try {
					stmt.execute();
				} catch (PSQLException e) {
					System.out.println(e.getMessage());
				}
			}
			routes.close();
			stmt.close();
		} catch (SQLException e1) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_ROUTES + ".\n" + e1.getMessage());
		} catch (Exception e) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_ROUTES + ".\n" + e.getMessage());
		}
	}
	
	private void updateSimpleIdTrips(Connection connection) {
		PreparedStatement stmt = null;
		
		try {
			stmt = connection.prepareStatement("select distinct trip_id from " + Schema.TRIPS
												+ " where trip_id not in (select trip_id from "
												+ Schema.SIMPLEID_TRIPS + " )");
			ResultSet trips = stmt.executeQuery();
			stmt = connection.prepareStatement("insert into " + Schema.SIMPLEID_TRIPS + " (trip_id) values(?)");
			while(trips.next()) {
				stmt.setString(1, trips.getString(1));
				try {
					stmt.execute();
				} catch (PSQLException e) {
					System.out.println(e.getMessage());
				}
			}
			trips.close();
			stmt.close();
		} catch (SQLException e1) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_TRIPS + ".\n" + e1.getMessage());
		} catch (Exception e) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_TRIPS + ".\n" + e.getMessage());
		}
	}
	
	private void updateSimpleIdServices(Connection connection) {
		PreparedStatement stmt = null;
		
		try {
			stmt = connection.prepareStatement("select distinct service_id from " + Schema.CALENDAR
												+ " where service_id not in (select service_id from "
												+ Schema.SIMPLEID_SERVICE + " )");
			ResultSet agencies = stmt.executeQuery();
			stmt = connection.prepareStatement("insert into " + Schema.SIMPLEID_SERVICE + " (service_id) values(?)");
			while(agencies.next()) {
				stmt.setString(1, agencies.getString(1));
				try {
					stmt.execute();
				} catch (PSQLException e) {
					System.out.println(e.getMessage());
				}
			}
			agencies.close();
			stmt.close();
		} catch (SQLException e1) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_SERVICE + ".\n" + e1.getMessage());
		} catch (Exception e) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_SERVICE + ".\n" + e.getMessage());
		}
	}
	
	private void updateSimpleIdShapes(Connection connection) {
		PreparedStatement stmt = null;
		
		try {
			stmt = connection.prepareStatement("select distinct shape_id from " + Schema.SHAPES
												+ " where shape_id not in (select shape_id from "
												+ Schema.SIMPLEID_SHAPES + " )");
			ResultSet shapes = stmt.executeQuery();
			stmt = connection.prepareStatement("insert into " + Schema.SIMPLEID_SHAPES + " (shape_id) values(?)");
			while(shapes.next()) {
				stmt.setString(1, shapes.getString(1));
				try {
					stmt.execute();
				} catch (PSQLException e) {
					System.out.println(e.getMessage());
				}
			}
			shapes.close();
			stmt.close();
		} catch (SQLException e1) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_SHAPES + ".\n" + e1.getMessage());
		} catch (Exception e) {
			System.err.println("Failed to update table " + Schema.SIMPLEID_SHAPES + ".\n" + e.getMessage());
		}
	}
}

