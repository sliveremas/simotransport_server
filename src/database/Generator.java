package database;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class Generator {
	public static final String delimiter = ";";
	public static final boolean useSimpleId = true;
	
	//File Names
	// Light Rail (LR)
	public static final String LIGHT_RAIL_FOLDER = Configurations.DOWNLOAD_FOLDER + File.separator + "LR";
	public static final String LIGHT_RAIL_FILE = "LR.csv";
	public static final String LIGHT_RAIL_ROUTES = "LR_Routes.csv";
	public static final String LIGHT_RAIL_CALENDAR = "LR_Calendar.csv";
	public static final String LIGHT_RAIL_STOPS = "LR_Stops.csv";
	public static final String LIGHT_RAIL_STOP_NAMES = "LR_Stop_Names.csv";
	public static final String LIGHT_RAIL_STOP_TIMES = "LR_Stops_Times.csv";
	public static final String LIGHT_RAIL_TRIPS = "LR_Trips.csv";
	
	// Ferries (FE)
	public static final String FERRIES_FOLDER = Configurations.DOWNLOAD_FOLDER + File.separator + "FE";
	public static final String FERRIES_FILE = "FE.csv";
	public static final String FERRIES_ROUTES = "FE_Routes.csv";
	public static final String FERRIES_CALENDAR = "FE_Calendar.csv";
	public static final String FERRIES_STOPS = "FE_Stops.csv";
	public static final String FERRIES_STOP_NAMES = "FE_Stop_Names.csv";
	public static final String FERRIES_STOP_TIMES = "FE_Stops_Times.csv";
	public static final String FERRIES_TRIPS = "FE_Trips.csv";
	
	// Trains (TR)
	public static final String TRAINS_FOLDER = Configurations.DOWNLOAD_FOLDER + File.separator + "TR";
	public static final String TRAINS_FILE = "TR.csv";
	public static final String TRAINS_ROUTES = "TR_Routes.csv";
	public static final String TRAINS_CALENDAR = "TR_Calendar.csv";
	public static final String TRAINS_STOPS = "TR_Stops.csv";
	public static final String TRAINS_STATIONS = "TR_Stations.csv";
	public static final String TRAINS_STOP_TIMES = "TR_Stops_Times.csv";
	public static final String TRAINS_TRIPS = "TR_Trips.csv";
	
	// Buses (SB)
	public static final String BUSES_FOLDER = Configurations.DOWNLOAD_FOLDER + File.separator + "SB";
	public static final String BUSES_FILE = "SB.csv";
	public static final String BUSES_ROUTES = "SB_Routes.csv";
	public static final String BUSES_CALENDAR = "SB_Calendar.csv";
	public static final String BUSES_STOPS = "SB_Stops.csv";
	public static final String BUSES_STOP_TIMES = "SB_Stops_Times.csv";
	public static final String BUSES_TRIPS = "SB_Trips.csv";
	public static final String BUSES_SUBURBS = "SB_Suburbs.csv";
	
	public void generateLightRail(String directory) {
		
		Connection connection = DBConnection.openNewConnection();
		System.out.println("Initiating the Light Rail File Generation Process");
		
		//Light_rail
		String LR_query = "select agency_name from agency where agency_id = 'LR'";

		//Light Rail Route
		String LR_Routes_query = "select route_id, route_long_name from routes where agency_id = 'LR'";
		String LR_Routes_query_simple = "select routeid.simple_id, r.route_long_name "
				+ "from routes r "
				+ "join simpleid_routes routeid on (routeid.route_id = r.route_id) "
				+ "where r.agency_id = 'LR'";
		
		//Light Rail Calendar 
		String LR_Calendar_query = "select distinct c.service_id, monday, tuesday, wednesday, thursday, "
				+ "friday, saturday, sunday from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join calendar c on (t.service_id = c.service_id) "
				+ "where a.agency_id='LR'";
		String LR_Calendar_query_simple = "select distinct serviceid.simple_id, monday, tuesday, wednesday, thursday, "
				+ "friday, saturday, sunday from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join calendar c on (t.service_id = c.service_id) "
				+ "join simpleid_service serviceid on (serviceid.service_id = c.service_id) "
				+ "where a.agency_id='LR'";
		
		//Light Rail Stops
		String LR_Stops_query = "select distinct ss.station_id, s.stop_name, station.station_lat, station.station_lon from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "join lr_stations_stops ss on (ss.stop_id = s.stop_id) "
				+ "join lr_stations station on (station.station_id = ss.station_id) "
				+ "where a.agency_id='LR'";
		// no need for simple id anymore
		String LR_Stops_query_simple = "select distinct ss.station_id, s.stop_name, station.station_lat, station.station_lon from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "join lr_stations_stops ss on (ss.stop_id = s.stop_id) "
				+ "join lr_stations station on (station.station_id = ss.station_id) "
				+ "where a.agency_id='LR'";
				
		//Light Rail Stop times
		String LR_Stop_times_query = "select st.trip_id, st.arrival_time, ss.station_id, st.stop_sequence "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join lr_stations_stops ss on (ss.stop_id = st.stop_id) "
				+ "where a.agency_id='LR'";
		
		// no need for simple id anymore
		String LR_Stop_times_query_simple = "select tripid.simple_id, st.arrival_time, ss.station_id, st.stop_sequence "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join simpleid_trips tripid on (tripid.trip_id = t.trip_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id)"
				+ "join lr_stations_stops ss on (ss.stop_id = st.stop_id) "
				+ "where a.agency_id='LR'";
		
		//Light Rail Stop_names
		String LR_Stop_names_query = "select distinct s.stop_name from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "where a.agency_id='LR'";
		
		//Light Rail Trips
		String LR_Trips = "select t.route_id, t.trip_id, t.service_id from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "where a.agency_id='LR'";
		String LR_Trips_simple = "select routeid.simple_id, tripid.simple_id, serviceid.simple_id from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join simpleid_routes routeid on (routeid.route_id = r.route_id) "
				+ "join simpleid_trips tripid on (tripid.trip_id = t.trip_id) "
				+ "join simpleid_service serviceid on (serviceid.service_id = t.service_id) "
				+ "where a.agency_id='LR'";
		
		generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_FILE, LR_query);
		if(useSimpleId) {
			generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_ROUTES, LR_Routes_query_simple);
			generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_CALENDAR, LR_Calendar_query_simple);
			generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_STOPS, LR_Stops_query_simple);
			generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_STOP_TIMES, LR_Stop_times_query_simple);
			generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_TRIPS, LR_Trips_simple);
		} else {
			generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_ROUTES, LR_Routes_query);
			generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_CALENDAR, LR_Calendar_query);
			generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_STOPS, LR_Stops_query);
			generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_STOP_TIMES, LR_Stop_times_query);
			generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_TRIPS, LR_Trips);
		}
		generateFile(connection, directory + File.separator + LIGHT_RAIL_FOLDER, LIGHT_RAIL_STOP_NAMES, LR_Stop_names_query);				

		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Light Rail File Generation Process is finished.");
	}
	
	public void generateFerries(String directory) {
		
		Connection connection = DBConnection.openNewConnection();
		System.out.println("Initiating the Ferries File Generation Process");
		
		// Ferries
		String FE_query = "select agency_name from agency where agency_id = '112'";
		
		// Ferries Routes
		String FE_Routes_query ="select route_id, route_long_name from routes where agency_id = '112'";
		String FE_Routes_query_simple = "select routeid.simple_id, r.route_long_name "
				+ "from routes r "
				+ "join simpleid_routes routeid on (routeid.route_id = r.route_id) "
				+ "where r.agency_id = '112'";
		
		// Ferries Calendar 
		String FE_Calendar_query = "select distinct c.service_id, monday, tuesday, wednesday, thursday, "
				+ "friday, saturday, sunday from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join calendar c on (t.service_id = c.service_id) "
				+ "where a.agency_id='112'";
		String FE_Calendar_query_simple = "select distinct serviceid.simple_id, monday, tuesday, wednesday, thursday, "
				+ "friday, saturday, sunday from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join calendar c on (t.service_id = c.service_id) "
				+ "join simpleid_service serviceid on (serviceid.service_id = c.service_id) "
				+ "where a.agency_id='112'";
		
		// Ferries Stops
		String FE_Stops_query = "select distinct s.stop_id, s.stop_name, s.stop_lat, s.stop_lon from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "join simpleid_stops stopid on (stopid.stop_id = s.stop_id) "
				+ "where a.agency_id='112'";
		String FE_Stops_query_simple = "select distinct stopid.simple_id, s.stop_name, s.stop_lat, s.stop_lon from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "join simpleid_stops stopid on (stopid.stop_id = s.stop_id) "
				+ "where a.agency_id='112'";
		 		
		// Ferries Stop_times
		String FE_Stop_times_query = "select st.trip_id, st.arrival_time, st.stop_id, st.stop_sequence "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id)"
				+ "where a.agency_id='112'";
		String FE_Stop_times_query_simple = "select tripid.simple_id, st.arrival_time, stopid.simple_id, st.stop_sequence "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join simpleid_trips tripid on (tripid.trip_id = t.trip_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id)"
				+ "join simpleid_stops stopid on (stopid.stop_id = st.stop_id) "
				+ "where a.agency_id='112'";
		
		// Ferries Stop names
		String FE_Stop_names_query = "select distinct s.stop_name from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "where a.agency_id='112'";
		
		// Ferries Trips
		String FE_Trips = "select t.route_id, t.trip_id, t.service_id from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) where a.agency_id='112'";
		String FE_Trips_simple = "select routeid.simple_id, tripid.simple_id, serviceid.simple_id from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join simpleid_routes routeid on (routeid.route_id = r.route_id) "
				+ "join simpleid_trips tripid on (tripid.trip_id = t.trip_id) "
				+ "join simpleid_service serviceid on (serviceid.service_id = t.service_id) "
				+ "where a.agency_id='112'";
		
		generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_FILE, FE_query);
		if(useSimpleId) {
			generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_ROUTES, FE_Routes_query_simple);
			generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_CALENDAR, FE_Calendar_query_simple);
			generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_STOPS, FE_Stops_query_simple);
			generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_STOP_TIMES, FE_Stop_times_query_simple);
			generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_TRIPS, FE_Trips_simple);
			
		} else {			
			generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_ROUTES, FE_Routes_query);
			generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_CALENDAR, FE_Calendar_query);
			generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_STOPS, FE_Stops_query);
			generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_STOP_TIMES, FE_Stop_times_query);
			generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_TRIPS, FE_Trips);
			
		}
		generateFile(connection, directory + File.separator + FERRIES_FOLDER, FERRIES_STOP_NAMES, FE_Stop_names_query);	
		
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Ferries File Generation Process is finished.");
	}
	
	public void generateTrains(String directory) {
		
		Connection connection = DBConnection.openNewConnection();
		System.out.println("Initiating the Trains File Generation Process");
		
		//Trains
		String TR_query = "select agency_name from agency where agency_id = 'x0001'";
		
		// Trains Routes
		String TR_Routes_query ="select route_id, route_long_name from routes where agency_id = 'x0001'";
		String TR_Routes_query_simple = "select routeid.simple_id, r.route_long_name "
				+ "from routes r "
				+ "join simpleid_routes routeid on (routeid.route_id = r.route_id) "
				+ "where r.agency_id = 'x0001'";
		// Trains Calendar 
		String TR_Calendar_query = "select distinct c.service_id, monday, tuesday, wednesday, thursday, "
				+ "friday, saturday, sunday from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join calendar c on (t.service_id = c.service_id) "
				+ "where a.agency_id='x0001'";
		String TR_Calendar_query_simple = "select distinct serviceid.simple_id, monday, tuesday, wednesday, thursday, "
				+ "friday, saturday, sunday from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join calendar c on (t.service_id = c.service_id) "
				+ "join simpleid_service serviceid on (serviceid.service_id = c.service_id) "
				+ "where a.agency_id='x0001'";
		
		// Trains Stop times
		String TR_Stop_times_query = "select st.trip_id, st.arrival_time, st.stop_id, st.stop_sequence "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "where a.agency_id='x0001'";
		String TR_Stop_times_query_simple = "select tripid.simple_id, st.arrival_time, stopid.simple_id, st.stop_sequence "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join simpleid_stops stopid on (stopid.stop_id = st.stop_id) "
				+ "join simpleid_trips tripid on (tripid.trip_id = st.trip_id) "
				+ "where a.agency_id='x0001'";
		
		// Trains Parent Stations
		String TR_parent_stations = "select stop_id, stop_name from stops "
				+ "where stop_id in (select distinct parent_station "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "where a.agency_id='x0001') order by stop_id";
		String TR_parent_stations_simple = "select stopid.simple_id, s.stop_name from stops s "
				+ "join simpleid_stops stopid on (stopid.stop_id = s.stop_id) "
				+ "where s.stop_id in (select distinct parent_station "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "where a.agency_id='x0001') order by s.stop_id";
		
		// Trains Platform / Stops
		String TR_Stops_query = "select distinct s.stop_id, substring(s.stop_name from '[0-9]$') as platform, s.parent_station, s.stop_lat, s.stop_lon "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "where a.agency_id='x0001'";
		String TR_Stops_query_simple = "select distinct stopid.simple_id, substring(s.stop_name from '[0-9]$') as platform, parentstopid.simple_id, s.stop_lat, s.stop_lon "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "join simpleid_stops stopid on (stopid.stop_id = s.stop_id) "
				+ "join simpleid_stops parentstopid on (parentstopid.stop_id = s.parent_station) "
				+ "where a.agency_id='x0001'";
		
		// Trains Trips
		String TR_Trips = "select t.route_id, t.trip_id, t.service_id from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) where a.agency_id='x0001'";

		String TR_Trips_simple = "select routeid.simple_id, tripid.simple_id, serviceid.simple_id from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join simpleid_routes routeid on (routeid.route_id = r.route_id) "
				+ "join simpleid_trips tripid on (tripid.trip_id = t.trip_id) "
				+ "join simpleid_service serviceid on (serviceid.service_id = t.service_id) "
				+ "where a.agency_id='x0001'";
		
		generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_FILE, TR_query);
		if(useSimpleId) {
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_ROUTES, TR_Routes_query_simple);
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_CALENDAR, TR_Calendar_query_simple);
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_STOP_TIMES, TR_Stop_times_query_simple);
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_STATIONS, TR_parent_stations_simple);				
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_STOPS, TR_Stops_query_simple);
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_TRIPS, TR_Trips_simple);
			
		} else {
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_ROUTES, TR_Routes_query);
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_CALENDAR, TR_Calendar_query);
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_STOP_TIMES, TR_Stop_times_query);
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_STATIONS, TR_parent_stations);				
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_STOPS, TR_Stops_query);
			generateFile(connection, directory + File.separator + TRAINS_FOLDER, TRAINS_TRIPS, TR_Trips);
		}
		
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Trains File Generation Process is finished.");
	}
	
	public void generateBuses(String directory) {
		
		Connection connection = DBConnection.openNewConnection();
		System.out.println("Initiating the Buses File Generation Process");
		
		// Buses
		String SB_query = "select agency_name from agency where agency_id = '11954'";
		
		// Buses Routes
		String SB_routes_query = "select r.route_id, r.route_short_name, r.route_long_name from routes r "
				+ "join agency a on (a.agency_id = r.agency_id) "
				+ "where a.agency_id='11954'";
		String SB_routes_query_simple = "select routeid.simple_id, r.route_short_name, r.route_long_name "
				+ "from routes r "
				+ "join simpleid_routes routeid on (r.route_id = routeid.route_id) "
				+ "join agency a on (a.agency_id = r.agency_id) "
				+ "where a.agency_id='11954'";
		
		// Buses Calendar 
		String SB_Calendar_query = "select distinct c.service_id, monday, tuesday, wednesday, thursday, "
				+ "friday, saturday, sunday from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join calendar c on (t.service_id = c.service_id) "
				+ "where a.agency_id='11954'";
		String SB_Calendar_query_simple = "select distinct serviceid.simple_id, monday, tuesday, wednesday, thursday, "
				+ "friday, saturday, sunday from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join calendar c on (t.service_id = c.service_id) "
				+ "join simpleid_service serviceid on (serviceid.service_id = c.service_id) "
				+ "where a.agency_id='11954'";
		
		// Buses Stop times
		String SB_Stop_times_query = "select st.trip_id, st.arrival_time, st.stop_id, st.stop_sequence "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "where a.agency_id='11954'";
		String SB_Stop_times_query_simple = "select tripid.simple_id, st.arrival_time, stopid.simple_id, st.stop_sequence "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join simpleid_trips tripid on (tripid.trip_id = t.trip_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join simpleid_stops stopid on (stopid.stop_id = st.stop_id) "
				+ "where a.agency_id='11954'";
				
		// Buses Stops
		String SB_Stops_query = "select distinct s.stop_id, s.stop_name, s.suburb_id, s.stop_lat, s.stop_lon "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "where a.agency_id='11954'";
		String SB_Stops_query_simple = "select distinct stopid.simple_id, s.stop_name, s.suburb_id, s.stop_lat, s.stop_lon "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "join simpleid_stops stopid on (stopid.stop_id = s.stop_id) "
				+ "where a.agency_id='11954'";
		
		// Buses Trips
		String SB_Trips = "select t.route_id, t.trip_id, t.service_id from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) where a.agency_id='11954'";
		String SB_Trips_simple = "select routeid.simple_id, tripid.simple_id, serviceid.simple_id from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join simpleid_routes routeid on (routeid.route_id = r.route_id) "
				+ "join simpleid_trips tripid on (tripid.trip_id = t.trip_id) "
				+ "join simpleid_service serviceid on (serviceid.service_id = t.service_id) "
				+ "where a.agency_id='11954'";
		
		// Suburbs
		String SB_Suburbs = "select distinct su.suburb_id, su.suburb_name "
				+ "from agency a join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "join suburbs su on (su.suburb_id = s.suburb_id) "
				+ "where a.agency_id='11954'";

		
		generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_FILE, SB_query);
		if(useSimpleId) {
			generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_ROUTES, SB_routes_query_simple);
			generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_CALENDAR, SB_Calendar_query_simple);
			generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_STOP_TIMES, SB_Stop_times_query_simple);
			generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_STOPS, SB_Stops_query_simple);
			generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_TRIPS, SB_Trips_simple);
			
		} else {
			generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_ROUTES, SB_routes_query);
			generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_CALENDAR, SB_Calendar_query);
			generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_STOP_TIMES, SB_Stop_times_query);
			generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_STOPS, SB_Stops_query);
			generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_TRIPS, SB_Trips);
			
		}
		generateFile(connection, directory + File.separator + BUSES_FOLDER, BUSES_SUBURBS, SB_Suburbs);
		
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Buses File Generation Process is finished.");
	}

	//Generate the SQL Query and save result to file
	public void generateFile(Connection connection, String directory, String filename, String query) {

		PreparedStatement stmt = null;		
		ResultSet rs = null;
		
		File file = new File(directory);
		if(directory != null && !directory.equals("") && !file.exists()) {
			file.mkdirs();
			System.out.println("Created folder " + file.getAbsolutePath());
		}
		file = new File(directory + File.separator + filename);		
		try {
//			if(file.exists()) {
//				file.delete();
//			}
			file.createNewFile();
			System.out.println("Created file " + file.getName());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
		    FileWriter fw = new FileWriter(file.getPath(), false); //the true will append the new data
			stmt = connection.prepareStatement(query);
			rs = stmt.executeQuery();
			String result = "";
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
//			for(int counter = 1; counter <= columnsNumber; counter++) {
//				if(result.equals("")) {
//					if(rsmd.getColumnLabel(counter) == null) {
//						result = "\"" + result + "\"";
//					} else {
//						result = "\""+ rsmd.getColumnLabel(counter) + "\"" ;
//					}
//				} else {
//					if(rsmd.getColumnLabel(counter) == null) {
//						result = result + delimiter + "\"\"";
//					} else {
//						result = result + delimiter + "\"" + rsmd.getColumnLabel(counter) + "\"";
//					}
//				}
//			}
//			result = result + "\n";
		    fw.write(result);//appends the string to the file
		    fw.flush();
			while(rs.next()) {
				result = "";
				for(int counter = 1; counter <= columnsNumber; counter++) {
					if(result.equals("")) {
						if(rs.getString(counter) == null) {
//							result = "\"" + result + "\"";
						} else {
//							result = "\""+ rs.getString(counter) + "\"" ;
							result = rs.getString(counter);
						}
					} else {
						if(rs.getString(counter) == null) {
//							result = result + ",\"\"";
							result = result + delimiter;
						} else {
//							result = result + delimiter + "\"" + rs.getString(counter) + "\"";
							result = result + delimiter + rs.getString(counter);
						}
					}
				}
				result = result + "\n";
			    fw.write(result);//appends the string to the file
			    fw.flush();
			}
		    fw.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(filename + " generated successfully!");
	}
}
