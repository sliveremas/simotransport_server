package database;

public class Schema {
	// Table Names
	// GTFS Tables
	public static final String AGENCY = "AGENCY";
	public static final String STOPS = "STOPS";
	public static final String ROUTES = "ROUTES";
	public static final String TRIPS = "TRIPS";
	public static final String STOP_TIMES = "STOP_TIMES";
	public static final String CALENDAR = "CALENDAR";
	public static final String CALENDAR_DATES = "CALENDAR_DATES";
	public static final String SHAPES = "SHAPES";
	
	// Added Information
	public static final String SUBURBS = "SUBURBS";
	public static final String SUBURBS_STOPS = "SUBURBS_STOPS";
	public static final String LR_STATIONS = "LR_STATIONS";
	public static final String LR_STATIONS_STOPS = "LR_STATIONS_STOPS";
	
	// Simple Id 
	public static final String SIMPLEID_AGENCY = "SIMPLEID_AGENCY";
	public static final String SIMPLEID_STOPS = "SIMPLEID_STOPS";
	public static final String SIMPLEID_ROUTES = "SIMPLEID_ROUTES";
	public static final String SIMPLEID_TRIPS = "SIMPLEID_TRIPS";
	public static final String SIMPLEID_SERVICE = "SIMPLEID_SERVICE";
	public static final String SIMPLEID_SHAPES = "SIMPLEID_SHAPES";
}
