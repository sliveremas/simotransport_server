package database;

import java.io.File;

public class Configurations {
//	public static String JDBC_PATH = "org.sqlite.JDBC";
//	public static String DATABASE_PATH = "jdbc:sqlite:server.db";
	
	public final static String JDBC_PATH = "org.postgresql.Driver";
	public final static String DATABASE_PATH = "jdbc:postgresql://localhost:5432/postgres";
	
	public final static String DB_USER = "postgres";
	public final static String DB_PASSWORD = "postgres";
	
	public final static String FILELIST_ATTRIB = "mostRecentData";
	public final static String DOWNLOAD_FOLDER = "Downloads";

	public static String SERVLET_DIRECTORY = ""; // This directory will be initialised when the app init

	public static String DOWNLOAD_PATH = SERVLET_DIRECTORY + DOWNLOAD_FOLDER;
	
}
