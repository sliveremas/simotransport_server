package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.postgresql.util.PSQLException;

import helper.geocode.GeocodingSuburb;

public class SuburbsImporter {
	
	public static void useGeoCodeToFillSuburbs() {
		Connection connection = null;
		try {
			connection = DBConnection.openNewConnection();
			Statement stmt = connection.createStatement();
			ResultSet stops = stmt.executeQuery("select stop_id, stop_lat, stop_lon from stops where suburb_id is null");
			PreparedStatement suburbInsert = connection.prepareStatement("insert into " + Schema.SUBURBS + "(suburb_name) values(?)");
			PreparedStatement suburbPrepStmt = connection.prepareStatement("insert into " + Schema.SUBURBS_STOPS + " (suburb, stop_id) values(?,?)");
			PreparedStatement pstmt = connection.prepareStatement("update stops set suburb_id=(select suburb_id from suburbs where suburb_name=?) "
					+ "where stop_id=?");
			while(stops.next()) {
				String stop_id = stops.getString(1);
				double lat = stops.getDouble(2);
				double lon = stops.getDouble(3);
				String suburb = GeocodingSuburb.getSuburb(lat, lon);
				if(suburb != null) {
					try {
						suburbInsert.setString(1, suburb);
						suburbInsert.execute(); 
					} catch (PSQLException e) {
						// This probably because the suburbs is there already.
					}
					try {
						suburbPrepStmt.setString(1, suburb);
						suburbPrepStmt.setString(2, stop_id);
						suburbPrepStmt.execute();
						
						pstmt.setString(1, suburb);
						pstmt.setString(2, stop_id);
						
						System.out.println(pstmt.toString());
						pstmt.execute();
					} catch (PSQLException e) {
//						connection.rollback();
						System.out.println(e.getMessage());
					}
				} else {
					System.out.println("Failed to add suburb information for stop_id " + stop_id);
				}
				
			}
			
			suburbInsert.close();
			suburbPrepStmt.close();
			pstmt.close();
			stmt.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
			e1.getNextException().printStackTrace();;
			System.out.println("Failed to open connection to database to check for empty suburb in stops table");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Failed to find orphan stops");
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
