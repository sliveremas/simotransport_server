﻿-- get agency_name for buses
select agency_name from agency where agency_id='11954';

-- get all the routes
select r.route_id, r.route_short_name, r.route_long_name from routes r
	join agency a on (a.agency_id = r.agency_id)
	where a.agency_id='11954';

-- get all the trips
select r.route_id, t.service_id, t.trip_id from routes r
	join agency a on (a.agency_id = r.agency_id)
	join trips t on (t.route_id = r.route_id)
	where a.agency_id='11954';

-- get all the stop times
select st.trip_id, st.arrival_time, st.stop_id, st.stop_sequence from routes r
	join agency a on (a.agency_id = r.agency_id)
	join trips t on (t.route_id = r.route_id)
	join stop_times st on (st.trip_id = t.trip_id)
	where a.agency_id='11954';

-- get all the stops
select distinct s.stop_id, s.stop_name, s.stop_lat, s.stop_lon from routes r
	join agency a on (a.agency_id = r.agency_id)
	join trips t on (t.route_id = r.route_id)
	join stop_times st on (st.trip_id = t.trip_id)
	join stops s on (s.stop_id = st.stop_id)
	where a.agency_id='11954';

-- Displays calendar of all services of Light Rail
select distinct c.service_id, monday, tuesday, wednesday, thursday, friday, saturday, sunday 
	from agency a 
	join routes r on (a.agency_id = r.agency_id) 
	join trips t on (t.route_id = r.route_id) 
	join calendar c on (t.service_id = c.service_id) 
	where a.agency_id='11954';
