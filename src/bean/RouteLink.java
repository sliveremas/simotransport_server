package bean;

public class RouteLink {

	private String routeLinkId;
	private String fromStopId;
	private String toStopId;
	
	public RouteLink(String id) {
		this.routeLinkId = id;
	}

	public String getRouteLinkId() {
		return this.routeLinkId;
	}

	public void setRouteLinkId(String routeLinkId) {
		this.routeLinkId = routeLinkId;
	}

	public String getFromStopId() {
		return this.fromStopId;
	}

	public void setFromStopId(String fromStopId) {
		this.fromStopId = fromStopId;
	}

	public String getToStopId() {
		return this.toStopId;
	}

	public void setToStopId(String toStopId) {
		this.toStopId = toStopId;
	}
}
