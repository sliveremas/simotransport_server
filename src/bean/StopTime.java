package bean;

public class StopTime {
	private String tripId;
	private int arrivalTime;
	private int departureTime;
	private String stopId;
	private int stopSequence;
	private String stopHeadsign;
	private int pickupType;
	private int dropOffType;
	private double shapeDistTraveled;
	
	public StopTime() { }
	public StopTime(String tripId, int arrivalTime, int departureTime, String stopId, int stopSequence,
			String stopHeadsign, int pickupType, int dropOffType, double shapeDistTraveled) {
		super();
		this.tripId = tripId;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
		this.stopId = stopId;
		this.stopSequence = stopSequence;
		this.stopHeadsign = stopHeadsign;
		this.pickupType = pickupType;
		this.dropOffType = dropOffType;
		this.shapeDistTraveled = shapeDistTraveled;
	}
	public String getTripId() {
		return tripId;
	}
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	public int getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(int departureTime) {
		this.departureTime = departureTime;
	}
	public String getStopId() {
		return stopId;
	}
	public void setStopId(String stopId) {
		this.stopId = stopId;
	}
	public int getStopSequence() {
		return stopSequence;
	}
	public void setStopSequence(int stopSequence) {
		this.stopSequence = stopSequence;
	}
	public String getStopHeadsign() {
		return stopHeadsign;
	}
	public void setStopHeadsign(String stopHeadsign) {
		this.stopHeadsign = stopHeadsign;
	}
	public int getPickupType() {
		return pickupType;
	}
	public void setPickupType(int pickupType) {
		this.pickupType = pickupType;
	}
	public int getDropOffType() {
		return dropOffType;
	}
	public void setDropOffType(int dropOffType) {
		this.dropOffType = dropOffType;
	}
	public double getShapeDistTraveled() {
		return shapeDistTraveled;
	}
	public void setShapeDistTraveled(double shapeDistTraveled) {
		this.shapeDistTraveled = shapeDistTraveled;
	}
	
}
