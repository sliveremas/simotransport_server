package helper.geocode;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import logic.CustomAuthenticator;

public class GeocodingSuburb {
	private static final String GEOCODING_URL = "https://maps.googleapis.com/maps/api/geocode/json?result_type=locality";
	private static final String RESULTS = "results";
	private static final String ADDRESS_COMPONENTS = "address_components";
	private static final String TYPES = "types";
	private static final String SHORT_NAME = "short_name";
	private static final String LOCALITY = "locality";
	
	public static String getSuburb(double lat, double lon) {
		String suburb = null;
		URL url = null;
		try {
			url = new URL(produceURLUsingAPI1(lat, lon));
		} catch (MalformedURLException e1) {
			System.err.println("Wrong url: " + e1.getMessage());
		}
		try(BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
			String inputLine;
			StringBuffer response = new StringBuffer();
			
			while((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			
			JSONObject obj = new JSONObject(response.toString());
			JSONArray results = obj.getJSONArray(RESULTS);
			int index = 0;
			while(index < results.length()) {
				JSONObject cur_obj = results.getJSONObject(index);
				JSONArray address_comp = cur_obj.getJSONArray(ADDRESS_COMPONENTS);
				for(int i = 0; i < address_comp.length(); i++) {
					JSONObject cur_address_comp = address_comp.getJSONObject(i);
					JSONArray types = cur_address_comp.getJSONArray(TYPES);
					for(int j = 0; j < types.length(); j++) {
						String curType = types.getString(j);
						if(curType.equals(LOCALITY)) {
							suburb = cur_address_comp.getString(SHORT_NAME);
							return suburb;
						}
					}
				}
			}
 		} catch (IOException e1) {
			System.out.println("Cannot send request to google geocode.");
		}
		return suburb;
	}
	
	private static String produceURLUsingAPI1(double lat, double lon) {
		StringBuilder sb = new StringBuilder(GEOCODING_URL);
		sb.append("&" + "latlng=" + lat + "," + lon);
		sb.append("&" + "key=" + CustomAuthenticator.GEOCODING_API_KEY1);
		String result = sb.toString();
		return result;
	}
	
	private static String produceURLUsingAPI2(double lat, double lon) {
		StringBuilder sb = new StringBuilder(GEOCODING_URL);
		sb.append("&" + "latlng=" + lat + "," + lon);
		sb.append("&" + "key=" + CustomAuthenticator.GEOCODING_API_KEY2);
		return sb.toString();
	}
	
	private static String produceURLUsingAPI3(double lat, double lon) {
		StringBuilder sb = new StringBuilder(GEOCODING_URL);
		sb.append("&" + "latlng=" + lat + "," + lon);
		sb.append("&" + "key=" + CustomAuthenticator.GEOCODING_API_KEY3);
		return sb.toString();
	}
	
}


