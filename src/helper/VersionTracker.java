package helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This class keeps track the version of the .zip file.
 * The version is kept on a txt file called version.txt
 * When the server was first initialized, getNewestVersion will read the version.txt
 * file. If no version.txt file on the directory, then set the version to 0000.00.00.
 * 
 * @author Julius Tio
 *
 */
public class VersionTracker {
	public final static String NULL_VERSION = "0000.00.00.00";
	
	private static String versionFileName = "version.txt";
	private static String versionFilePath = ""; // This is the default filename
	// should be updated to the application directory at the start of the application
	// to avoid inconsistency version
	
	private static String currentVersion = null;
	
	public static String getCurrentVersion() {
		if(currentVersion == null) {
			// When the program first started, newestVersion has no value
			File versionFile = new File(versionFilePath + File.separator + versionFileName);
			if(!versionFile.exists()) {
				// no version.txt ever specified. return 0000.00.00
				writeVersionToFile(versionFile, NULL_VERSION);
			} else {
				// if there is a file, look into that file
				try {
					BufferedReader br = new BufferedReader(new FileReader(versionFile));
					currentVersion = br.readLine();
					br.close();
				} catch(IOException e) {
					System.err.println("Cannot read versioning textfile on "+versionFile.getAbsolutePath());
				}
			}
		}
		return currentVersion;
	}
	
	private static void writeVersionToFile(File versionFile, String value) {
		try {
			if(!versionFile.exists()) {
				versionFile.createNewFile();
				System.out.println("Version File is created at " + versionFile.getAbsolutePath());
			} else {
				System.out.println("Updating Version file, located at " + versionFile.getAbsolutePath());
			}
			FileWriter fw = new FileWriter(versionFile, false);
			fw.write(value);
			fw.write("\n");
			fw.flush();
			fw.close();
			currentVersion = value;
		} catch (IOException e) {
			System.err.println("Cannot write versioning textfile on "+versionFile.getAbsolutePath());
		}
	}
	public static boolean updateVersion(String version) {
		if(!version.equals(currentVersion)) {
			writeVersionToFile(new File(versionFilePath + File.separator + versionFileName), version);
			currentVersion = version;
			System.out.println("Update Version to " + version);
		}
		return true;
	}

	public static void setFilePath(String versionFilePath) {
		VersionTracker.versionFilePath = versionFilePath;
	}
}
