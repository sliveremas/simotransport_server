package helper;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/*
 * ZipHandler handles everything with zip and unzip.
 */
public class ZipHandler {

	static final int BUFFER = 2048;
	static final String outputFolder = "extractedFiles";
	
	/**
	 * Recursively remove files and directories specified in the directory.
	 * @param directory The directory to be removed
	 * @return
	 */
	public static boolean removeFiles (File directory) {
		if (directory != null) {
			String[] list = directory.list();
			if (list != null) {
				for (int i = 0; i < list.length; i++) {
					File entry = new File(directory, list[i]);
					if (entry.isDirectory()) {
						if (!removeFiles(entry))
							return false;
					} else {
						if (!entry.delete())
							return false;
					}
				}
			}
			return directory.delete();
		} else {
			return false;
		}
	}

	/**
	 * Checks if the file has .xml extension
	 * @param fileName
	 * @return
	 */
	public static boolean isXML (String fileName) {
		if (fileName.lastIndexOf(".") == -1) {
			return false;
		} else {
			// get rid of backup xml files
			int count = fileName.length() - fileName.replace(".", "").length();
			if (count == 1) {
				int length = fileName.length();
				if(fileName.substring(length - 3, length).equalsIgnoreCase("xml")) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}
	}

	/**
	 * Unzipping given zip file into specified outputDirectory
	 * @param zipFile
	 * @param outputDirectory
	 * @return
	 */
	public static LinkedList<File> unZip (String zipFile, String outputDirectory) {
		System.out.println("Start unZip");
		byte[] buffer = new byte[BUFFER];
		LinkedList<File> filePaths = new LinkedList<File>();

		try {
			// Create output directory if not exist.
			// Otherwise, deletes existing directory then create it.
			File directory = new File(outputDirectory + File.separator + ZipHandler.outputFolder);
			if (!directory.exists()) {
				directory.mkdir();
			} else {
				// Clear old files inside the folder (to avoid duplicates)
//				removeFiles(directory);
				directory.mkdir();
			}
			// Get the zip file content
			ZipInputStream input = new ZipInputStream(new FileInputStream(zipFile));
			// Get the zipped file list entry
			ZipEntry entry = input.getNextEntry();
			while (entry != null) {
				String fileName = entry.getName();
				File newFile = new File(outputDirectory + File.separator + ZipHandler.outputFolder + File.separator + fileName);
				// System.out.println(newFile.getAbsolutePath() + " is created");
				if (entry.isDirectory()) {
					new File(newFile.getParent()).mkdir();
				} else {
					filePaths.add(newFile.getAbsoluteFile());
					new File(newFile.getParent()).mkdirs();
					FileOutputStream output = new FileOutputStream(newFile, false);
					int i;
					while ((i = input.read(buffer)) > 0) {
						output.write(buffer, 0, i);
					}
					output.close();
				}
				entry = input.getNextEntry();
			}
			input.closeEntry();
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Finish unZip");
		return filePaths;
	}
	
	/**
	 * Zip a directory (and all files inside it), or a file depending of the directory argument.
	 * @param directory Can be a directory or a file
	 * @param outputZipFile location of the output zipped file
	 * @return File object of the zipped file, or null if the directory arg does not exist or outputZipFile is write-protected
	 */
	public static File zip(String directory, String outputZipFile) {
		File source = new File(directory);
		if(!source.exists())
		{
			return null;
		}

		File outputFile;
		// if source is a folder, then create a .zip file on the parent directory
		// just put it in the same directory otherwise
		if(source.isDirectory()) {
			File parentFile = new File(directory);
			outputFile = new File(parentFile.getParentFile() + File.separator + outputZipFile);
		} else {
			outputFile = new File(directory + File.separator + outputZipFile);
		}
//		if(outputFile.exists()) {
//			outputFile.delete();
//			try {
//				outputFile.createNewFile();
//			} catch (IOException e) {
//				System.out.println("Failed to create new file in " + outputZipFile);
//				e.printStackTrace();
//				return null;
//			}
//		}
		
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(outputFile, false);
		} catch (FileNotFoundException e) {
			System.out.println(outputZipFile + " does not exist.");
			e.printStackTrace();
			return null;
		}
		ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(fos));
		BufferedInputStream origin;
		byte[] data = new byte[BUFFER];
		try {
			if(source.isFile()) {
				System.out.println("Zipping a single file: " + source.getAbsolutePath() + " to " + outputFile.getAbsolutePath());
				FileInputStream fis = new FileInputStream(source);
				origin = new BufferedInputStream(fis, BUFFER);
				// creating zip entry
				ZipEntry entry = new ZipEntry(source.getName());
				try {
					zos.putNextEntry(entry);

					int count;
					while((count = origin.read(data, 0, BUFFER)) != -1) {
						zos.write(data, 0, count);
					} 

				} catch (IOException e) {
					System.out.println("Cannot write zip entry : " + entry.getName());

				} finally {	
					if(origin != null)
						origin.close();
				}

			} else {
				// otherwise, it is a folder
				File[] files = source.listFiles();
				for(File f : files) {
					System.out.println("Adding file " + f.getName() + " into " + outputZipFile);
					FileInputStream fis = new FileInputStream(f);
					origin = new BufferedInputStream(fis, BUFFER);
					// creating zip entry
					ZipEntry entry = new ZipEntry(f.getName());
					try {
						zos.putNextEntry(entry);

						int count;
						while((count = origin.read(data, 0, BUFFER)) != -1) {
							zos.write(data, 0, count);
						} 

					} catch (IOException e) {
						System.out.println("Cannot write zip entry : " + entry.getName());

					} finally {	
						if(origin != null)
							origin.close();
					}	
				}
			}
		} catch(IOException e) {
			System.out.println("Cannot create zip file : " + outputZipFile);
		} finally {
			if(zos != null)
				try {
					zos.close();
				} catch (IOException e) {
					// Some previous error happened.
				}
		}
		return outputFile;
	}
	
	public static boolean isMD5Same(ZipFile zip1, ZipFile zip2) throws Exception {
		if(MD5Checksum.getMD5Checksum(zip1.getName()).equals(MD5Checksum.getMD5Checksum(zip2.getName()))) {
			return true;
		} else {
			return false;
		}
	}
	
	public static HashMap<String, Long> makeZipMap(ZipFile zip) {
		HashMap<String, Long> map = new HashMap<String, Long>();

		for (Enumeration<? extends ZipEntry> e = zip.entries(); e.hasMoreElements();) {
			ZipEntry entry = (ZipEntry) e.nextElement();
			String filename = entry.getName();
			Long timestamp = entry.getTime();

			if (!map.containsKey(filename)) {
				map.put(filename, timestamp);
				// System.out.println(filename + ":" + timestamp);
			}
		}
		return map;
	}
	
	public static boolean areEntriesSame(ZipFile zip1, ZipFile zip2) {
		HashMap<String, Long> map1 = null; // map of file name to its timestamp
		HashMap<String, Long> map2 = null;
	
		System.out.println("Creating map for zip 1...");
		map1 = makeZipMap(zip1);
		System.out.println("Creating map for zip 2...");
		map2 = makeZipMap(zip2);
	
		if(map1 != null && map2 != null && !map1.equals(map2)) {
			return false;
		} else {
			return true;
		}
	}
	
	public static void removeZip(String path) {
		System.out.println("Trying to remove: " + path);
		Path p = Paths.get(path);
		try {
			Files.deleteIfExists(p);
		} catch (IOException e) {
			System.err.println("Unable to delete zip!");
			e.printStackTrace();
		}
	}

	public static boolean isSizeSame(ZipFile zip1, ZipFile zip2) {
		if (zip1.size() != zip2.size()) {
			return false;
		} else {
			return true;
		}
	}
	

	// check if malformed or incompletely downloaded
	public static boolean isZipMalformed(String path) {
		boolean isMalformed = false;

		System.out.println("path = " + path);
		try {
			ZipFile zip = new ZipFile(new File(path));
			zip.close();
		} catch (ZipException e) {
			System.err.println("zip exception");
			isMalformed = true;
		} catch (IOException e) {
			System.err.println("zip io exception");
			isMalformed = true;
		}

		return isMalformed;
	}
}

