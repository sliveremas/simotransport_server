package helper.parser;

import bean.StopTime;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import database.DBConnection;
import database.Schema;

/**
 * ArrivalTimeSmoother helps estimate the time of arrival time when data in stop_times
 * from tdx does not contain the arrival time. 
 * @author Julius Tio
 *
 */
public class ArrivalTimeSmoother {
	public static void SmoothArrivalTime() {
		Connection connection = null;
		try {
			connection = DBConnection.openNewConnection();
			connection.setAutoCommit(false);
			Statement stmt = connection.createStatement();
			// Get all trips with empty arrival time
			ResultSet trips = stmt.executeQuery("select distinct trip_id from stop_times where arrival_time is null;");
			PreparedStatement ps = connection.prepareStatement("select trip_id, stop_sequence, arrival_time, departure_time from stop_times where trip_id = ? order by stop_sequence");
			while(trips.next()) {
				String currentTripId = trips.getString(1);
				
				// get all stop_times for that current trip
				ps.setString(1, currentTripId);
				ResultSet stopTimesResult = ps.executeQuery();
				ArrayList<StopTime> stopTimes = new ArrayList<StopTime>(); 
				while(stopTimesResult.next()) {
					StopTime st = new StopTime();
					st.setTripId(stopTimesResult.getString("trip_id"));
					st.setStopSequence(stopTimesResult.getInt("stop_sequence"));
					st.setArrivalTime(stopTimesResult.getInt("arrival_time"));
					st.setDepartureTime(stopTimesResult.getInt("departure_time"));
					stopTimes.add(st);
				}
				
				// do calculation and update arrival time
				calculateArrivalTimes(stopTimes);
				
				// write back to db
				persistChanges(stopTimes, connection);
			}
			connection.commit();
			ps.close();
			stmt.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
			e1.getNextException().printStackTrace();;
			System.out.println("Failed to open connection to database to check for empty arrival times in stop_times table");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Failed to find empty arrival times");
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static void persistChanges(ArrayList<StopTime> stopTimes, Connection connection) {
		try {
			PreparedStatement ps = connection.prepareStatement("update " + Schema.STOP_TIMES + " set arrival_time = ?, departure_time = ?"
					+ " where trip_id = ? and stop_sequence = ?");
			
			for(int i = 0; i < stopTimes.size(); i++) {
				StopTime currentStopTime = stopTimes.get(i);
				ps.setInt(1, currentStopTime.getArrivalTime());
				ps.setInt(2, currentStopTime.getDepartureTime());
				ps.setString(3, currentStopTime.getTripId());
				ps.setInt(4, currentStopTime.getStopSequence());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Failed to update new arrival time to database.");
		}
	}

	private static void calculateArrivalTimes(ArrayList<StopTime> stopTimes) {
		int startEmptyIndex = -1;
		for(int i = 0; i < stopTimes.size(); i++) {
			StopTime currentStopTime = stopTimes.get(i);
			if(currentStopTime.getArrivalTime() == 0) {
				// if it is new empty slot, start counting
				if(startEmptyIndex == -1) {
					startEmptyIndex = i;
				}
			} else {
				// if startEmptyIndex is not -1, that means previous stopTime is empty
				if(startEmptyIndex != -1) {
					// process the empty index
					int nEmpty = i - startEmptyIndex + 1;
					int lastKnownArrivalTime = stopTimes.get(startEmptyIndex-1).getArrivalTime();
					int nextKnownArrivalTime = stopTimes.get(i).getArrivalTime();
					int gapValue = ((nextKnownArrivalTime - lastKnownArrivalTime) / nEmpty);
					int nextArrivalTime = lastKnownArrivalTime + gapValue;
					for(int j = startEmptyIndex; j < i; j++) {
						stopTimes.get(j).setArrivalTime(nextArrivalTime);
						stopTimes.get(j).setDepartureTime(nextArrivalTime);
						nextArrivalTime += gapValue;
					}
					
					startEmptyIndex = -1;
				}
			}
		}
	}
	
}
