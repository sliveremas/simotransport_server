package helper.parser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import database.DBConnection;
import database.Schema;

/**
 * LightRailStopsCombiner combines two platform of different direction but has the same name
 * into a same simple stop_id
 * @author Julius Tio
 *
 */
public class LightRailStopsCombiner {
	private static void initializeDatabaseLR_stops(Connection connection) {
		try {
			connection = DBConnection.openNewConnection();
			String createQuery = "CREATE TABLE IF NOT EXISTS ";

			//CREATE TABLES
			String attributes = " (station_id serial primary key,"
					+ "station_name TEXT,"
					+ "station_lat float,"
					+ "station_lon float)";
			PreparedStatement stmt = connection.prepareStatement(createQuery + Schema.LR_STATIONS + attributes);
			stmt.execute();
			stmt.close();

			System.out.println("Created table: " + Schema.LR_STATIONS);
			
			attributes = " (station_id int,"
					+ "stop_id TEXT,"
					+ "primary key(station_id, stop_id)) ";			
			stmt = connection.prepareStatement(createQuery + Schema.LR_STATIONS_STOPS + attributes);
			stmt.execute();
			stmt.close();

			System.out.println("Created table: " + Schema.LR_STATIONS_STOPS);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}  finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public static void addNewLightRailStops() {
		Connection connection = null;
		try {
			connection = DBConnection.openNewConnection();
			
			// Add table if it does not exist
			initializeDatabaseLR_stops(connection);
			
			Statement stmt = connection.createStatement();
			connection.setAutoCommit(false);
			// Get all stops that are not in LR_STATIONS_STOPS yet
			ResultSet stops = stmt.executeQuery("select distinct s.stop_id, s.stop_name, s.stop_lat, s.stop_lon from agency a "
				+ "join routes r on (a.agency_id = r.agency_id) "
				+ "join trips t on (t.route_id = r.route_id) "
				+ "join stop_times st on (t.trip_id = st.trip_id) "
				+ "join stops s on (s.stop_id = st.stop_id) "
				+ "left outer join " + Schema.LR_STATIONS_STOPS + " ss on (ss.stop_id = s.stop_id) "
				+ "where a.agency_id='LR' and s.stop_id not in (select stop_id from lr_stations_stops)");
			
			PreparedStatement psFindStation = connection.prepareStatement("select station_id, station_lat, station_lon from " + Schema.LR_STATIONS 
					+ " where station_name = ? limit 1");
			PreparedStatement psInsertStationStop = connection.prepareStatement(""
					+ "insert into " + Schema.LR_STATIONS_STOPS + "(station_id, stop_id) "
							+ "values(?,?) ");
			PreparedStatement psInsertStation = connection.prepareStatement(""
					+ "insert into " + Schema.LR_STATIONS + "(station_name, station_lat, station_lon) "
							+ "values(?,?,?) ", Statement.RETURN_GENERATED_KEYS);
			PreparedStatement psUpdateStation = connection.prepareStatement(
					"update " + Schema.LR_STATIONS + " SET station_lat = ?, station_lon = ? where station_id = ? "
					);
			while(stops.next()) {
				String currentStopId = stops.getString(1);
				String currentStopName = stops.getString(2);
				double currentStopLat = stops.getDouble(3);
				double currentStopLon = stops.getDouble(4);
				
				// get the station for that stop
				psFindStation.setString(1, currentStopName);
				ResultSet stationResult = psFindStation.executeQuery();
				
				int stationId = 0;
				// if station is not found, then insert row to LR_STATIONS
				// else update the location to the midpoint of the two
				if(!stationResult.next()) {
					psInsertStation.setString(1, currentStopName);
					psInsertStation.setDouble(2, currentStopLat);
					psInsertStation.setDouble(3, currentStopLon);
					psInsertStation.executeUpdate();
					ResultSet keys = psInsertStation.getGeneratedKeys();
					keys.next();
					stationId = keys.getInt("station_id");
				} else {
					stationId = stationResult.getInt("station_id");
					double stationLat = stationResult.getDouble("station_lat");
					double stationLon = stationResult.getDouble("station_lon");
					stationLat = (stationLat + currentStopLat) /2;
					stationLon = (stationLon + currentStopLon) /2;
					//update the midpoint
					psUpdateStation.setDouble(1, stationLat);
					psUpdateStation.setDouble(2, stationLon);
					psUpdateStation.setInt(3, stationId);
					psUpdateStation.executeUpdate();
				}
				// insert row to LR_STATIONS_STOPS
				psInsertStationStop.setInt(1, stationId);
				psInsertStationStop.setString(2, currentStopId);
				psInsertStationStop.executeUpdate();

			}
			psFindStation.close();
			psInsertStation.close();
			psInsertStationStop.close();
			stmt.close();
			connection.commit();
		} catch (SQLException e1) {
			e1.printStackTrace();
			e1.getNextException().printStackTrace();;
			System.out.println("Failed to open connection to database to check for empty arrival times in stop_times table");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Failed to find empty arrival times");
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}