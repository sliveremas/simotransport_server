package helper.parser;

import java.io.File;
import java.util.List;

import database.Generator;
import database.Importer;

public class GTFSParser {
	public void importFilesToDatabase (List<File> filePaths, String directory) {
		System.out.println("Start importing data to database.");
		
		Importer importer = new Importer();
		
		for (File file: filePaths) {
			if(file.getName().endsWith("agency.txt")) {
				importer.importAgency(file.getAbsolutePath());
				System.out.println("Import agency finished");
			} else if(file.getName().endsWith("routes.txt")) {
				importer.importRoutes(file.getAbsolutePath());
				System.out.println("Import routes finished");
			} else if(file.getName().endsWith("trips.txt")) {
				importer.importTrips(file.getAbsolutePath());
				System.out.println("Import trips finished");
			} else if(file.getName().endsWith("stops.txt")) {
				importer.importStops(file.getAbsolutePath());
				System.out.println("Import stops finished");
			} else if(file.getName().endsWith("stop_times.txt")) {
				importer.importStopTimes(file.getAbsolutePath());
				System.out.println("Import stop_times finished");
			} else if(file.getName().endsWith("calendar.txt")) {
				importer.importCalendar(file.getAbsolutePath());
				System.out.println("Import calendar finished");
			} else if(file.getName().endsWith("calendar_dates.txt")) {
				importer.importCalendarDates(file.getAbsolutePath());
				System.out.println("Import calendar_dates finished");
			} else if(file.getName().endsWith("shapes.txt")) {
				importer.importShapes(file.getAbsolutePath());
				System.out.println("Import shapes finished");
			} else {
				System.out.println("Unknown file ignored: " + file.getAbsolutePath());
			}
		}
		System.out.println("Finished adding data to database.");
		
		System.out.println("Updating Simple Id data");
		importer.updateSimpleId();
		System.out.println("Finished updating simple id data.");
	}

	public void generateCSVFiles(String directory) {
		System.out.println("Generating .csv file.");
		
		Generator generator = new Generator();
		
		generator.generateLightRail(directory);
		generator.generateFerries(directory);
		generator.generateTrains(directory);
		generator.generateBuses(directory);
		
		System.out.println("Finished generating .csv files.");
	}
}
