package helper.file;

import java.io.File;

public interface FileHandler {
	public File[] handleFile(File raw);
	public File[] getData();
}
