package helper.file;

import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import database.Configurations;
import database.DBConnection;
import database.Generator;
import database.SuburbsImporter;
import helper.VersionTracker;
import helper.ZipHandler;
import helper.parser.ArrivalTimeSmoother;
import helper.parser.GTFSParser;
import helper.parser.LightRailStopsCombiner;
import helper.parser.TXCParser;

/**
 * DataHandler is the main entry point of data processing. After the zip file has been downloaded from TDX server, 
 * DataHandler.handleData() should be called to unzip, inserting it to database and compress those files into smaller format 
 * @author Julius Tio
 *
 */
public class DataHandler {

	private static final String FILE_EXTENSION = "zip"; // the name of the file extension to be handled by getExtractedData()

	public static final String LIGHT_RAIL_ZIP = "LR." + FILE_EXTENSION;
	public static final String FERRIES_ZIP = "FE." + FILE_EXTENSION;
	public static final String TRAINS_ZIP = "TR." + FILE_EXTENSION;
	public static final String BUSES_ZIP = "SB." + FILE_EXTENSION;
	public DataHandler() { }

	public File[] getZippedFiles() {
		// Points the working directory to the EXTRACTED_FOLDER
		File folder = new File(Configurations.SERVLET_DIRECTORY + Configurations.DOWNLOAD_FOLDER);
		System.out.println("Extracted Folder path is " + Configurations.SERVLET_DIRECTORY + Configurations.DOWNLOAD_FOLDER);
		
		// List all the files in the folder
		File[] listOfFiles = folder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if(name.lastIndexOf('.')>0)
				{
					// get last index for '.' char
					int lastIndex = name.lastIndexOf('.');

					// get extension
					String str = name.substring(lastIndex + 1);

					// match path name extension
					if(str.equals(FILE_EXTENSION))
					{
						return true;
					}
				}
				return false;
			}
		});
		
	    return listOfFiles;
	}
	
	public File[] handleFile(File gtfsSource, File txcSource) {
		System.out.println("Start handling data from source");
		
		// Unzip the downloaded source file
		List<File> gtfsFilePaths = ZipHandler.unZip(gtfsSource.getAbsolutePath(), gtfsSource.getParentFile().getPath());
		List<File> txcFilePaths = ZipHandler.unZip(txcSource.getAbsolutePath(), txcSource.getParentFile().getPath());
		
//		DBConnection.initDB();
//		DBConnection.initSimpleIdTables();
		System.out.println("Database is generated");
		
		// Parse the unzipped files i.e. import to the database from CSV files and create smaller data format
		GTFSParser gtfsParser = new GTFSParser();
//		gtfsParser.importFilesToDatabase(gtfsFilePaths, Configurations.SERVLET_DIRECTORY);

		TXCParser txcParser = new TXCParser();
//		txcParser.extractSuburbsInformationFromTXC(txcFilePaths);

//		SuburbsImporter.useGeoCodeToFillSuburbs();
		
		ArrivalTimeSmoother.SmoothArrivalTime();
		
		LightRailStopsCombiner.addNewLightRailStops();
		
		gtfsParser.generateCSVFiles(Configurations.SERVLET_DIRECTORY);
		
		// Zip the created smaller data format
		// Zip Light Rail (LR) into LR.zip
		ZipHandler.zip(Configurations.SERVLET_DIRECTORY + File.separator + Generator.LIGHT_RAIL_FOLDER, LIGHT_RAIL_ZIP);
		// Zip Trains (TR) into TR.zip
		ZipHandler.zip(Configurations.SERVLET_DIRECTORY + File.separator + Generator.FERRIES_FOLDER, FERRIES_ZIP);
		// Zip Trains (TR) into TR.zip
		ZipHandler.zip(Configurations.SERVLET_DIRECTORY + File.separator + Generator.TRAINS_FOLDER, TRAINS_ZIP);
		// Zip Buses (SB) into SB.zip
		ZipHandler.zip(Configurations.SERVLET_DIRECTORY + File.separator + Generator.BUSES_FOLDER, BUSES_ZIP);
		
		// Update to current version
		updateVersion();
		
		return getZippedFiles();
	}

	private void updateVersion() {
		// get the date of the download
		Calendar c = Calendar.getInstance(TimeZone.getTimeZone("Australia/Sydney"));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
		String oldVersion = VersionTracker.getCurrentVersion();
		String newVersion = sdf.format(c.getTime());
		if(oldVersion.startsWith(newVersion)) {
			String revisionText = oldVersion.substring(oldVersion.lastIndexOf('.')+1, oldVersion.length());
			Integer revision = Integer.valueOf(revisionText) + 1;
			newVersion = newVersion.concat("." + String.valueOf(revision));
		} else {
			newVersion = newVersion.concat(".00");
		}
		VersionTracker.updateVersion(newVersion);
	}
	

	public File[] getData() {
		return getZippedFiles();
	}
	
	public static String getCurrentSunday() {
		Calendar c = Calendar.getInstance();
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		return new SimpleDateFormat("yyyyMMdd").format(c.getTime());
	}
}
