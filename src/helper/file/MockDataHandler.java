package helper.file;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

public class MockDataHandler implements FileHandler {
	
	private String testFile1;
	private String testFile2;
	private String testFile3;

	public MockDataHandler() {
		Calendar cal = Calendar.getInstance();
		StringBuilder sb = new StringBuilder();
		sb.append("simo.");
		sb.append(cal.get(Calendar.YEAR));
		int month = cal.get(Calendar.MONTH);
		month++;
		sb.append(month);
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		if (dayOfMonth < 10) {
			sb.append("0");	
		}
		sb.append(dayOfMonth);
		sb.append(".zip");
		testFile1 = sb.toString();
		testFile2 = "simo.init.zip";
		testFile3 = "simo.patch.20140912.zip";
	}
	
	@Override
	public File[] getData() {
		return handleFile(null);
	}

	@Override
	public File[] handleFile(File raw) {
		File[] fileList = new File[3];
		File newFile = new File(testFile1);
		File newFile2 = new File(testFile2);
		File newFile3 = new File(testFile3);

		fileList[0]=newFile;
		fileList[1]=newFile2;
		fileList[3]=newFile3;
		return fileList;
	}
	
}