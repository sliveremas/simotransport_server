package helper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import au.com.bytecode.opencsv.CSVReader;

/**
 * CSVLoader imports CSV files into Database
 * @author Julius Tio
 * @author Sagar Sethi
 * 
 */
public class CSVLoader {


	private final int BATCH_SIZE = 1000000;
	
	private static final 
		String SQL_INSERT = "INSERT INTO ${table}(${keys}) VALUES(${values})";
	private static final String TABLE_REGEX = "\\$\\{table\\}";
	private static final String KEYS_REGEX = "\\$\\{keys\\}";
	private static final String VALUES_REGEX = "\\$\\{values\\}";

	private Connection connection;
	private char delimiter;

	/**
	 * Public constructor to build CSVLoader object with
	 * Connection details. The connection is closed on success
	 * or failure.
	 * @param connection
	 */
	public CSVLoader(Connection connection) {
		this.connection = connection;
		//Set default separator
		this.delimiter = ',';
	}
	
	/**
	 * Parse CSV file using OpenCSV library and load in 
	 * given database table. 
	 * @param csvFile Input CSV file
	 * @param tableName Database table name to import data
	 * @param truncateBeforeLoad Truncate the table before inserting 
	 * 			new records.
	 * @throws Exception
	 */
	public void loadCSV(String csvFile, String tableName,
			boolean truncateBeforeLoad, ColumnType... dataTypes) throws Exception {

		CSVReader csvReader = null;
		if(null == this.connection) {
			throw new Exception("Not a valid connection.");
		}
		try {
			csvReader = new CSVReader(new FileReader(csvFile), this.delimiter);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error occured while executing file. " + e.getMessage());
		}

		String[] headerRow = csvReader.readNext();
		
		// Bug in the downloaded GTFS zip file where each file was appended with unkown string
		// in windows there are 3 unknown characters on each file
		// in mac there is 1 unknown charater on each file
		String osName = System.getProperty("os.name");
		if(osName.startsWith("Windows")) {
			// Remove the unknown first 3 characters
			headerRow[0] = headerRow[0].substring(3, headerRow[0].length());
		} else if(osName.startsWith("Mac")) {
			// Remove the unknown first character
			headerRow[0] = headerRow[0].substring(1, headerRow[0].length());
		}
		
		if (headerRow == null) {
			throw new FileNotFoundException("No columns are defined in given CSV file." +
											"Please check the CSV file format.");
		}

		String questionmarks = StringUtils.repeat("?,", headerRow.length);
		questionmarks = (String) questionmarks.subSequence(0, questionmarks.length() - 1);

		String query = SQL_INSERT.replaceFirst(TABLE_REGEX, tableName);
		query = query.replaceFirst(KEYS_REGEX, StringUtils.join(headerRow, ","));
		query = query.replaceFirst(VALUES_REGEX, questionmarks);

		System.out.println("Query: " + query);

		String[] nextLine;
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = this.connection;
			con.setAutoCommit(false);
			ps = con.prepareStatement(query);

			if(truncateBeforeLoad) {
				//delete data from table before loading csv
				con.createStatement().execute("DELETE FROM " + tableName);
			}

			int count = 0;
			Date date = null;
			while ((nextLine = csvReader.readNext()) != null) {

				if (null != nextLine) {
					int index = 1;
					int counter = 0;
					for (String string : nextLine) {

						if(string.equals("")) {
							ps.setNull(index++, Types.NULL);
						}else if(dataTypes[counter].equals(ColumnType.String)) {
							ps.setString(index++, string);
						} else if(dataTypes[counter].equals(ColumnType.Integer)) {
								ps.setInt(index++, Integer.parseInt(string));
						} else if(dataTypes[counter].equals(ColumnType.Float)) {
							ps.setFloat(index++, Float.parseFloat(string));
						} else if(dataTypes[counter].equals(ColumnType.Date)) {
							date = DateUtil.convertToDate(string);
							ps.setDate(index++, new java.sql.Date(date.getTime()));						
						} else if(dataTypes[counter].equals(ColumnType.Time)) {
							int millisec = 3600* Integer.parseInt(string.substring(0,2)) +
											60*  Integer.parseInt(string.substring(3,5)) +
											     Integer.parseInt(string.substring(6,8));
							ps.setInt(index++, millisec);
						} else {
							System.out.println("I REACH HERE. Counter: " +counter);
						}
						counter++;
					}
					ps.addBatch();
				}
				if (++count % BATCH_SIZE == 0) {
					ps.executeBatch();
				}
			}
			ps.executeBatch(); // insert remaining records
			con.commit();
		} catch (Exception e) {
			con.rollback();
			e.printStackTrace();
			throw new Exception(
					"Error occured while loading data from file to database."
							+ e.getMessage());
		} finally {
			if (null != ps)
				ps.close();
			if (null != con)
				con.close();

			csvReader.close();
		}
	}

	public char getDelimiter() {
		return delimiter;
	}

	public void setSeparator(char delimiter) {
		this.delimiter = delimiter;
	}

}
