package helper;

public enum ColumnType {
	String, Integer, Date, Float, Time;
}
