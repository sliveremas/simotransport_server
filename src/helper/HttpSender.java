package helper;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class HttpSender {

	private static final String USER_AGENT = "Mozilla/5.0";
	
	public static HttpURLConnection sendRequest(String url) {
		URL obj = null;
		HttpURLConnection con = null;
		try {
			obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", USER_AGENT);
			

			int responseCode = con.getResponseCode();
			if(responseCode < 200 || responseCode >= 300) {
				System.err.println("Geocode server send " + responseCode);
			}

			System.out.println("Sending request to URL : " + url);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} 
		
		return con;
	}
}
