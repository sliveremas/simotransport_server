﻿select agency_name from agency where agency_id = 'LR';


--select distinct s.stop_id, s.stop_name, s.stop_lat, s.stop_lon from agency a join routes r on (a.agency_id = r.agency_id) 
--			join trips t on (t.route_id = r.route_id) 
--			join stop_times st on (t.trip_id = st.trip_id)
--			join stops s on (s.stop_id = st.stop_id)
--			where a.agency_id='LR';


--Light Rail Stop Names
--select distinct s.stop_name from agency a join routes r on (a.agency_id = r.agency_id) 
--			join trips t on (t.route_id = r.route_id) 
--			join stop_times st on (t.trip_id = st.trip_id)
--			join stops s on (s.stop_id = st.stop_id)
--			where a.agency_id='LR';

-- Displays the stop times of various trips of Light Rail
--select st.trip_id, st.arrival_time, st.stop_id, st.stop_sequence from agency a join routes r on (a.agency_id = r.agency_id) 
--			join trips t on (t.route_id = r.route_id) 
--			join stop_times st on (t.trip_id = st.trip_id)
--			where a.agency_id='LR';

-- Displays calendar of all services of Light Rail
--select c.service_id, monday, tuesday, wednesday, thursday, friday, saturday, sunday from agency a join routes r on (a.agency_id = r.agency_id) 
--			join trips t on (t.route_id = r.route_id) 
--			join calendar c on (t.service_id = c.service_id) where a.agency_id='LR';

-- Trips
--select trip_id, service_id from agency a join routes r on (a.agency_id = r.agency_id) 
--			join trips t on (t.route_id = r.route_id) where a.agency_id='LR';
