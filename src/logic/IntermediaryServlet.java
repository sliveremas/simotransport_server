package logic;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.nio.file.Path;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.Configurations;
import helper.VersionTracker;

/**
 * Servlet implementation class IntermediaryServlet
 */
@WebServlet("/IntermediaryServlet")
public class IntermediaryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public IntermediaryServlet() { }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		File[] TDXData = (File[]) getServletContext().getAttribute(Configurations.FILELIST_ATTRIB);
		
		// handle with error message
		if (TDXData == null) {
			PrintWriter writer = response.getWriter();

			response.setHeader("status", "error");
			response.setContentType("text/html");

			writer.write("<p>Server is not ready. Please wait </p>");

		} else {
			// send url of file to phone
			ServletContext ctx = getServletContext();

			response.setHeader("status", "ready");
			response.setContentType("text/html");
			
			String path;
			PrintWriter writer = response.getWriter();
			
			writer.write("<p>Version = ");
			writer.write(VersionTracker.getCurrentVersion());
			writer.write("</p>\n");
			
			for(File f : TDXData) {
				path = getPath(ctx, f.toPath());
				writer.write("<p>" + path.toString() + "</p>\n");
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private String getPath(ServletContext ctx, Path p)
			throws MalformedURLException {
		return ctx.getContextPath() + "/" + Configurations.DOWNLOAD_FOLDER + "/" + p.getFileName();
	}

}
