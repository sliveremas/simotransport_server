package logic;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import javax.servlet.ServletContext;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import database.Configurations;
import helper.ZipHandler;
import helper.file.DataHandler;
import helper.parser.TXCParser;

public class CheckTDX implements Job {

	private static String DOWNLOAD_GTFS_LINK = "https://tdx.transportnsw.info/download/files/full_greater_sydney_gtfs_static.zip";
	private static String DOWNLOAD_TXC_LINK = "https://tdx.transportnsw.info/download/files/transxchange.zip";
	private static final int RETRY_TIME = 60000;
	private static int buffer = 2048;
	private static String current;
	private static String gtfsPath_cur = ""; // variable for file that app users will receive
	private static String gtfsPath_new = ""; // newly downloaded file is assigned here
	
	private static String txcPath = "";
	private static ServletContext ctx;
	private static String timezone;

	public CheckTDX() { }
	
	public static void setDownloadPath(String realPath) {
		File file = new File(realPath);

		System.out.println("creating downloads folder at "
				+ file.getAbsolutePath());
		if (!file.exists()) {
			try {
				file.mkdir();
				System.out
						.println("successfully created 'Downloads' directory");
			} catch (SecurityException se) {
				System.err
						.println("security exception error, unable to create downloads folder");
			}
		} else {
			System.out.println("directory already exists");
		}

		current = file.getAbsolutePath();
		System.out.println("current path = " + current);
	}

	/**
	 * A thread task to download the GTFS file from TDX, compare and decide whether the new file is different than the old one
	 */
	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		System.out.println("executing task");
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		ctx = (ServletContext) dataMap.get("context");
		timezone = dataMap.getString("timezone");

		if (ctx == null) {
			System.err.println("context unable to be initialized");
		} else if (timezone == null) {
			System.err.println("timezone unable to be obtained");
		}

		System.out.println("Downloading file from tdx");
		mockDownloadGTFS(); // download file from TDX
//		downloadGTFS();
		
		while (!gtfsPath_new.equals("") && ZipHandler.isZipMalformed(gtfsPath_new)) {
			System.out.println("new zip file is malformed, redownloading");
			downloadGTFS();
		}

		System.out.println("path of zip1 is: " + gtfsPath_cur);
		System.out.println("path of zip2 is: " + gtfsPath_new);

		// if path1 (ie. file1) doesn't exist, this means the file downloaded is
		// the first one for the server or the server was just turned on
		if (gtfsPath_new.equals("")) {
			mockDownloadTXC();
//			downloadTXC();
			System.out.println("handling " + gtfsPath_cur);
			handleFile();
		} else { // there exists an old file and a new one has just been
					// downloaded
			boolean isBothFileSame = false;
			ZipFile zip1 = null;
			ZipFile zip2 = null;
			try {
				zip1 = new ZipFile(new File(gtfsPath_cur));
				zip2 = new ZipFile(new File(gtfsPath_new));
				
				// Find file differences
				isBothFileSame = ZipHandler.isSizeSame(zip1, zip2);
				if (isBothFileSame) {
					//compareEntries is slower than md5checkshum
					//compareEntries(zip1, zip2);
					isBothFileSame = ZipHandler.isMD5Same(zip1, zip2);
				}

			} catch (ZipException e) { // zip malformed - eg. server died while
										// dl'ing
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				System.err.println("Failed to compare file");
				e.printStackTrace();
			} finally {
				try {
					if(zip1 != null) {
						zip1.close();
					}
				} catch (Exception e) { }// should be safe to ignore this exception
				try {
					if(zip2 != null) {
						zip2.close();
					}
				} catch (Exception e) { }// should be safe to ignore this exception 
			}

			System.out.println("cleaning up extra zip");
			// clean up files such that only most recent version remains
			try {
				handleZip2(!isBothFileSame);
			} catch (IOException e) {
				e.printStackTrace();
			}

			if(!isBothFileSame) {
				// Downloading new TXC file to get the suburbs
//				File txc = downloadTXC();
				
				// Comment the top line and uncomment this one if you already have TXC file in your folder
				File txc = mockDownloadTXC();
				
				// Only handle file (ie. extract, drop tables, reinsert data, create rows, create new zip etc)
				// if there is a different
				System.out.println("handling " + gtfsPath_cur);
				handleFile();
			} else {
				System.out.println("No difference on the new downloaded file. Only set the list now");
				setListOfFilesForApp();
			}

		}

	}
	
	/**
	 * This function downloads the TransXChange file from the TDX server.
	 * The TransXChange file is useful only for creating connection between stops and suburbs
	 */
	private File downloadTXC() {
		File file = null;
		try {
			System.out.println("Downloading TransXChange from " + DOWNLOAD_TXC_LINK);
			Authenticator.setDefault(new CustomAuthenticator());
			URL url = new URL(DOWNLOAD_TXC_LINK);
			
			file = new File(current, "TXC.zip");
			// if filename exists, add number to the end
			// set path1 to point to existing temp
			if (file.exists() && !file.isDirectory()) {
				file.delete();
			}

			InputStream input = url.openStream();
			
			downloadFile(input, file);
			input.close();
			System.out.println("Finished downloading TransXChange file!");

			txcPath = file.getCanonicalPath();
			
		} catch (UnknownHostException e) {
			try {
				Thread.sleep(RETRY_TIME);
				downloadTXC();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
	
	private void downloadGTFS() {
		try {
			Authenticator.setDefault(new CustomAuthenticator());
			URL url = new URL(DOWNLOAD_GTFS_LINK);
			InputStream input = url.openStream();
			
			File file = new File(current, "GTFS_cur.zip");
			// if filename exists, add number to the end
			// set path1 to point to existing temp
			if (file.exists() && !file.isDirectory()) {
				gtfsPath_cur = file.getCanonicalPath();
				// if zip1 is malformed, it will be overwritten
				if (!ZipHandler.isZipMalformed(gtfsPath_cur)) {
					file = new File(current, "GTFS_new.zip");
					gtfsPath_new = file.getCanonicalPath();
					System.out.println("saving to " + gtfsPath_new);
				} else {
					System.out.println("malformed zip detected, overwriting");
					System.out.println("saving to " + gtfsPath_cur);
				}
			} else { // otherwise save as temp - ie. first data pull from tdx
						// server
				gtfsPath_cur = file.getCanonicalPath();
			}

			downloadFile(input, file);
			input.close();
			System.out.println("Finished downloading GTFS file!");
		} catch (UnknownHostException e) {
			try {
				Thread.sleep(RETRY_TIME);
				downloadGTFS();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	/**
	 * This method mimic the activity of downloading the GTFS file from the gov
	 * except that it does not download it. helpful to avoid download waiting time.
	 * This also set the path only to path1. Leaving it path2 empty, which in this case
	 * means there is no previously download .zip file from TDX
	 */
	private void mockDownloadGTFS() {
		try {
			File file = new File(current, "GTFS_cur.zip");
			gtfsPath_cur = file.getCanonicalPath();
//			File file2 = new File(current, "GTFS_new.zip");
//			path2 = file2.getCanonicalPath();
			System.out.println("done!");
		} catch (UnknownHostException e) {
			try {
				Thread.sleep(RETRY_TIME);
				downloadGTFS();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private File mockDownloadTXC() {
		File file = null;
		file = new File(current, "TXC.zip");
		try {
			this.txcPath = file.getCanonicalPath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
	// this function has been moved to txc parser
//	private boolean addSuburbsInformationFromTXC() {
//		File file = new File(current, "TXC.zip");
//		if(!file.exists()) {
//			return false;
//		}
//		List<File> filePaths = ZipHandler.unZip(file.getAbsolutePath(), file.getParentFile().getPath());
//		TXCParser parser = new TXCParser();
//		parser.extracSuburbsInformationFromTXC(filePaths);
//		return true;
//	}
	private void downloadFile(InputStream input, File file)
			throws FileNotFoundException, IOException {
		if(!file.exists()) {
			file.createNewFile();
		}
		OutputStream output = new FileOutputStream(file);

		int read = 0;
		byte[] bytes = new byte[buffer];

		while ((read = input.read(bytes)) > 0) {
			output.write(bytes, 0, read);
		}

		output.close();
	}

	private void handleZip2(boolean replace) throws IOException {
		Path p1 = Paths.get(gtfsPath_cur);
		Path p2 = Paths.get(gtfsPath_new);

		if (replace) { // old zip superceeded by new zip, ie. replace old copy
			// TODO: THERE IS A CHANCE THAT THIS FILE WILL BE IN USE
			// BY THE SERVLET OR EXTRACTOR
			// NEED TO PUT A LOCK ON IT OR SOMETHING
			// BEFORE IT CAN BE REPLACED BY NEWER VERSION
			// Suggest: check until the file is moveable
			System.out.println("new file is different to old one");
			System.out.println("replacing old file with new one");
			Files.move(p2, p1, REPLACE_EXISTING);
		} else { // new zip is same as old zip, ie. it's useless
			System.out.println("newly downloaded file is same as old one");
			System.out.println("removing duplicated version");
			removeZip(gtfsPath_new);
		}
	}

	private void removeZip(String p) {
		ZipHandler.removeZip(p);
	}

	// handle extraction of new file
	public void handleFile() {
		// save timestamp of new update/version of data
		Path p = Paths.get(gtfsPath_cur);
		Path txc = Paths.get(txcPath);
		File[] extracted = new DataHandler().handleFile(p.toFile(), txc.toFile());
		
		System.out.println("setting context attributes");
		System.out.println("Passed file lists in checkTDX. Number of files = " + extracted.length);
		ctx.setAttribute(Configurations.FILELIST_ATTRIB, extracted);
	}

	public void setListOfFilesForApp() {
		System.out.println("setting context attributes");
		
		File[] listOfFiles = new DataHandler().getZippedFiles();
		System.out.println("Passed file lists in checkTDX. Number of files = " + listOfFiles.length);
		ctx.setAttribute(Configurations.FILELIST_ATTRIB, listOfFiles);
	}
}
