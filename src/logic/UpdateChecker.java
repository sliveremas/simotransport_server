package logic;

import java.io.File;
import java.util.Calendar;
import java.util.TimeZone;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import database.Configurations;
import helper.VersionTracker;
import helper.file.DataHandler;

@WebListener
public class UpdateChecker implements ServletContextListener {

	// change the timezone as appropriate
	private static final String TIME_ZONE = "Australia/Sydney";
	private static Calendar c = Calendar.getInstance(TimeZone
			.getTimeZone(TIME_ZONE)); // used for storing time of last update
	// change these two values for implementation
	// these preset values were used for testing purposes
	private static int UPDATE_HOUR = c.get(Calendar.HOUR_OF_DAY); 
	private static final int UPDATE_MIN = (c.get(Calendar.MINUTE) + 1) % 60;

	private static Scheduler scheduler;
	private static ServletContext ctx;

	// used for testing
	public void checkTDXDataForUpdate() {
		CheckTDX task = new CheckTDX();

		SchedulerFactory factory = new StdSchedulerFactory();
		try {
			System.out.println("creating new job");
			JobDetail job = JobBuilder.newJob(task.getClass())
					.withIdentity("checkTDX", "group1").build();
			job.getJobDataMap().put("context", ctx);
			job.getJobDataMap().put("timezone", TIME_ZONE);
			System.out.println("done!");

			if(UPDATE_MIN == 1) {
				UPDATE_HOUR =+ 1;
			}
			System.out.println("creating trigger for the job");
			System.out.println("task set to be run at " + UPDATE_HOUR + ":"
					+ UPDATE_MIN + " every day");
			Trigger trigger = TriggerBuilder
					.newTrigger()
					.withIdentity("trigger1", "group1")
					.withSchedule(
							CronScheduleBuilder
									.dailyAtHourAndMinute(UPDATE_HOUR,
											UPDATE_MIN)
									.inTimeZone(TimeZone.getTimeZone(TIME_ZONE))
									.withMisfireHandlingInstructionFireAndProceed())
					.forJob("checkTDX", "group1").build();
			System.out.println("done!");

			System.out.println("creating schedule for the job");
			// schedule the job
			scheduler = factory.getScheduler();
			scheduler.start();
			scheduler.scheduleJob(job, trigger);
			System.out.println("done!");

		} catch (SchedulerException e) {
			e.printStackTrace();
		}

	}

	@Override
	// used by servlet context/container
	public void contextInitialized(ServletContextEvent event) {
		ctx = event.getServletContext();
		Configurations.SERVLET_DIRECTORY = ctx.getRealPath("");
		if(!Configurations.SERVLET_DIRECTORY.endsWith(File.separator)) {
			Configurations.SERVLET_DIRECTORY = Configurations.SERVLET_DIRECTORY.concat(File.separator);
		}
		VersionTracker.setFilePath(Configurations.SERVLET_DIRECTORY);
		CheckTDX.setDownloadPath(Configurations.SERVLET_DIRECTORY + Configurations.DOWNLOAD_FOLDER);
		checkLocalData();
		checkTDXDataForUpdate(); // schedule TDX update. Execute once a day
	}

	private void checkLocalData() {
		System.out.println("Checking local data");
		if(!VersionTracker.getCurrentVersion().equals(VersionTracker.NULL_VERSION)) {
			File[] files = new DataHandler().getZippedFiles();
			ctx.setAttribute(Configurations.FILELIST_ATTRIB, files);
			System.out.println("Local data exists. Version = " + VersionTracker.getCurrentVersion());
		} else {
			System.out.println("Local data does NOT exist. Version = " + VersionTracker.getCurrentVersion());
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		try {
			scheduler.shutdown();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

}
