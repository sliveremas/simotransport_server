﻿--select agency_name from agency --where agency_id = 'x0001';


--select distinct s.stop_id, s.stop_name, s.stop_lat, s.stop_lon from agency a join routes r on (a.agency_id = r.agency_id) 
--			join trips t on (t.route_id = r.route_id) 
--			join stop_times st on (t.trip_id = st.trip_id)
--			join stops s on (s.stop_id = st.stop_id)
--			where a.agency_id='x0001';


--Trains Parent Station
select stop_id, stop_name from stops 
	where stop_id in (select distinct parent_station from agency a join routes r on (a.agency_id = r.agency_id) 
			join trips t on (t.route_id = r.route_id) 
			join stop_times st on (t.trip_id = st.trip_id)
			join stops s on (s.stop_id = st.stop_id)
			where a.agency_id='x0001') order by stop_id;

--Trains Platform (or stops)
select distinct s.stop_id, substring(s.stop_name from '[0-9]$') as platform, s.parent_station
			from agency a join routes r on (a.agency_id = r.agency_id) 
			join trips t on (t.route_id = r.route_id) 
			join stop_times st on (t.trip_id = st.trip_id)
			join stops s on (s.stop_id = st.stop_id)
			where a.agency_id='x0001';

-- Displays the stop times of various trips of Light Rail
--select st.trip_id, st.arrival_time, st.stop_id, st.stop_sequence from agency a join routes r on (a.agency_id = r.agency_id) 
--			join trips t on (t.route_id = r.route_id) 
--			join stop_times st on (t.trip_id = st.trip_id)
--			where a.agency_id='x0001';

-- Displays calendar of all services of Light Rail
--select distinct c.service_id, monday, tuesday, wednesday, thursday, friday, saturday, sunday from agency a join routes r on (a.agency_id = r.agency_id) 
--			join trips t on (t.route_id = r.route_id) 
--			join calendar c on (t.service_id = c.service_id) where a.agency_id='x0001

-- Trips
--select trip_id, service_id from agency a join routes r on (a.agency_id = r.agency_id) 
--			join trips t on (t.route_id = r.route_id) where a.agency_id='x0001';
